const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Email = require('mongoose-type-mail');
const mongoosePaginate = require('mongoose-paginate');
let usersSchema = new Schema({
    fullname: String,
    username: {type: String, required: [true], unique: [true]},
    email: {type: Email, required: [true], unique: [true]}
});
usersSchema.plugin(mongoosePaginate);

let Users = mongoose.model('users', usersSchema);

module.exports = Users;