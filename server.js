const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();
const Router = require('./routes/router');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://admin:admin1234@ds229295.mlab.com:29295/heroku_t9v3lbdc');
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("DB connection alive");
});
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/', express.static(__dirname + '/frontend/public'));
app.use(Router);

app.listen(80, () => {
    console.log('Server started on: 80');
});