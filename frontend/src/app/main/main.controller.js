export class MainController {
  constructor ($scope, Restangular, $stateParams, $state, toastr) {
    'ngInject';
    $scope.user = {};
    $scope.edit = {};
    let users = Restangular.all('users');
    Restangular.setDefaultRequestParams('get', {limit: $stateParams.limit, page: $stateParams.page });
    users.customGET().then((users) => {
      $scope.users = users.docs;
      $scope.limit = users.limit;
      $scope.currentPage = users.page;
      $scope.pages = Array(users.pages).fill().map((e,i)=>i+1);
      toastr.success('Loading');
    });
    $scope.deleteUser = (id) => {
      Restangular.one('users', id).remove().then((user) => {
        if(user.error) {
          toastr.error(user.error)
        } else {
          $scope.users = $scope.users.filter((item) => {
            return item._id !== user._id
          });
          toastr.success('Delete user ' + user._id + ' Done!');
          $state.transitionTo('home', {page: $scope.page, limit: $scope.limit});
        }
      })
    };
    $scope.createUser = (user) => {
      users.post(user).then((user) => {
        if(user.error) {
          toastr.error(user.error)
        } else {
          $scope.users.push(user);
          $state.transitionTo('home', {page: $scope.page, limit: $scope.limit});
          $scope.user = {};
          toastr.success('Create user ' + user._id + ' Done!');
        }
      });
    };
    $scope.editUser = (id) => {
      $scope.edit[id] = true
    };
    $scope.cancelEditUser = (id) => {
      $scope.edit[id] = false
    };
    $scope.updateUser = (user) => {
      users.customPUT(user, user._id).then((user) => {
        if(user.error) {
          toastr.error(user.error)
        } else {
          toastr.success('Update user ' + user._id + ' Done');
          $scope.edit[user._id] = false
        }
      });
    };
    $scope.goToPage = (page, limit) => {
      $state.transitionTo('home', {page: page, limit: limit})
    }
  }
}
