export function config ($logProvider, toastrConfig, RestangularProvider) {
  'ngInject';
  // Enable log
  $logProvider.debugEnabled(true);

  RestangularProvider.setBaseUrl('http://localhost/api/v1');
  // Set options third-party lib
  toastrConfig.allowHtml = false;
  toastrConfig.timeOut = 900;
  toastrConfig.positionClass = 'toast-top-right';
  toastrConfig.preventDuplicates = false;
  toastrConfig.progressBar = true;
}
