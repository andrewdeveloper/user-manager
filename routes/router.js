const express = require('express');
const Users = require('./users');

let Router = express.Router();

Router.get('/api/v1/users', Users.GET);
Router.post('/api/v1/users', Users.POST);
Router.put('/api/v1/users/:id', Users.PUT);
Router.delete('/api/v1/users/:id', Users.DELETE);

module.exports = Router;