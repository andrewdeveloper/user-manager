const Users = require('../models/users');
module.exports = {
    GET: (req, res) => {
        let page = 1;
        let limit = 10;
        if(req.query.page) {
            page =+ req.query.page
        }
        if(req.query.limit) {
            limit =+ req.query.limit
        }
        Users.paginate({}, { page: page, limit: limit }).then((users) => {
            res.status(200).json(users)
        }).catch((error) => {
            res.send({error: error})
        });
    },
    POST: (req, res) => {
        let user = new Users(req.body);
        user.save()
            .then((user) => {
                res.status(200).json(user)
            })
            .catch((error) => {
                res.send({error: error})
            });
    },
    PUT: (req, res) => {
        Users.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true})
            .then((user => {
                res.status(200).json(user)
            }))
            .catch((error) => {
                res.send({error: error})
            });
    },
    DELETE: (req, res) => {
        Users.findByIdAndRemove(req.params.id)
            .then((user) => {
                res.status(200).json(user)
            })
            .catch((error) => {
                res.send({error: error})
            })
    }
};