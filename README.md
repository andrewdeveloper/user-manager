# User-Manager #

CRUD service (front-end + back-end) where you can add, remove and edit users.

### What i need to run this project? ###

* Node.js
* bower `npm install bower`
* Gulp `npm install gulp`
* Git :D

### Well, i have all that, what's next? ###

Do next steps:

* Clone this Repo `https://bitbucket.org/andrewdeveloper/user-manager.git`
* instal node dependencies`npm install`
* instal node dependencies in frontend folder`npm install`
* install bower dependencies in frontend `bower install`
* build the angular app in frontend folder `gulp`
* run the server `npm start`
* look in brower on localhost:80
* for development on angular app run `gulp serve` in fronend folder


### Cool, what's next? ###

Happy Coding ;)